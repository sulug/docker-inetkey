#!/bin/bash
#get host id from env variable 
hostID="$HOST_ID"
targetURL="$CHANNEL"
frequency="$FREQUENCY"
#uncomment link for debug
#targetURL="https://outlook.office.com/webhook/51af600d-ef27-478e-9631-94b8265d48f6@a6fa3b03-0a3c-4258-8433-a120dffcd348/IncomingWebhook/40b461ac36844242b48a65c98321d52f/c3d90fd3-6421-4652-a066-80aaf66f3ee5"
state="connected"

if [ $frequency = ""]; then
        frequency="5m"
fi

#check for hostID
if [ $hostID = ""]; then
        echo "Critical Failure. No host id specified."
        exit 1
fi

# check for channel url
if [ $targetURL = ""]; then
        echo "Critical Failure. No channel specified."
        exit 1
fi

#check the internet every 5 minutes
while true
do
        response=$(curl -X GET -o /dev/null --silent --write-out '%{http_code}\n' http://connectivity-check.ubuntu.com)
        # we get 204 on succes which is no content but 200 (blank page) when inetkey blocks the request
        if [[ $response -eq 204 ]]; then
                state="connected"
        else
        #send one message to teams when a connectivity error is detected
                if [ $state = "connected" ]; then
                        dataString="{\"@context\":\"https:\/\/schema.org\/extensions\",\"@type\":\"MessageCard\",\"themeColor\":\"0072C6\",\"title\":\"Inetkey has failed on a host\",\"text\":\"Host "$hostID" can not connect through the firewall\"}"
                        curl -X POST --header 'Content-Type: application/json' -d "$dataString" $CHANNEL 
                        state="disconnected"
                fi
        fi
        sleep $frequency
done