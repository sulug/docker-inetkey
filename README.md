## Inetkey Docker container

This docker container checks whether its host can connect through inetkey or whether inetkey has failed
for some reason. It will then notify the user on microsoft teams.

### Building

run

```
docker build -t <image_name_here> .
```

in the directory containing the Dockerfile

### Running

The script expects the environment variable `HOST_ID` to be set when it is run.

The container can therfor be started as follows

```
docker run --name <container_name_here> -d -e HOST_ID="HOST ID HERE" -e $CHANNEL="Your channel url here" $FREQUENCY="frequency here" <image_name_here>
```
The host id will be used to identify which host had an internet failure in the report on teams.

The channel url is required to send notification messages to teams.

The current frequency for checking the connection defualts to once every 5 minutes but this can also be adjusted in the script. This can be changed by setting an environment variable in the same format that is accepted by the sleep command.