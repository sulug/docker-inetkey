FROM reg.cs.sun.ac.za/computer-science/docker-prebuild/sun-ubuntu:18.04

RUN apt-get update && \
apt-get upgrade -y && \
apt-get install -y wget curl && \
apt-get autoremove && \
apt-get clean -y

ADD ./scripts ./

ENTRYPOINT "./internetcheck.sh"